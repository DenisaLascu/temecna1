using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;

namespace Server
{
    public class NameService : Greeter.GreeterBase
    {
        private readonly ILogger<NameService> _logger;
        public NameService(ILogger<NameService> logger)
        {
            _logger = logger;
        }

        public override Task<NameReply> SayHello(NameRequest request, ServerCallContext context)
        {
            System.Console.WriteLine(request.Name + " ");

            return Task.FromResult(new NameReply
            {
                //Message = request.Name + " "
                NameReply_ = request.Name + " "
            });
        }
    }
}
